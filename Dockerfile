FROM ycezard/nodejs
MAINTAINER Yann Cézard "yann.cezard@objectif-libre.com"
ENV REFRESHED_AT 2015-03-25
WORKDIR /opt
RUN git clone https://github.com/magne4000/quassel-webserver.git
WORKDIR /opt/quassel-webserver
RUN npm install --production
ADD settings.js /opt/quassel-webserver/
EXPOSE 8080
ENTRYPOINT ["node", "app.js", "-m", "http", "-p", "8080"]
CMD [""]
